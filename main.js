//--- generate overlay html
var elemDiv = document.createElement('div');
elemDiv.classList.add('overlay')
document.body.appendChild(elemDiv);

//--- menu search action
const getSearchButton = document.querySelector('#search')
const getSearchCloseButton = document.querySelector('#search span img')
// search active on click
getSearchButton.addEventListener('click', function () {
    this.classList.add('search-clicked')
    document.body.classList.add('search-open')
    setTimeout(() => {
        document.querySelector('input').focus()
    }, 600)
});

// Search close on click
getSearchCloseButton.addEventListener('click', function (e) {
    getSearchButton.classList.remove('search-clicked')
    document.body.classList.remove('search-open')
    e.stopPropagation()
});

//--- set menu width based on open menu
const getMenuWidth = document.querySelector('.menu__bottom__main-items').clientWidth
document.querySelector('.menu__bottom__dropdown').style.width = getMenuWidth + 'px'
window.addEventListener('resize', () => {
    const getMenuWidth = document.querySelector('.menu__bottom__main-items').clientWidth
    document.querySelector('.menu__bottom__dropdown').style.width = getMenuWidth + 'px'
})

//--- on click dropdown action
// on click get menu number & show that number of dropdown menu
const getMenuItems = document.querySelectorAll('.menu__bottom__main-items li:not(#search)')
const getDropdownItems = document.querySelectorAll('.menu__bottom__dropdown')
getMenuItems.forEach((e, i) => {
    e.addEventListener('click', () => {
        if (!e.classList.contains('dropdown-click')) {
            if (getDropdownItems?.[i]) {
                document.querySelector('.overlay').classList.add('active')
                getDropdownItems?.[i]?.classList.add('open-dropdown')
                e.classList.add('dropdown-click')
                document.body.classList.add('dropdown-active')
            }

        } else {
            getDropdownItems?.[i]?.classList.remove('open-dropdown')
            e.classList.remove('dropdown-click')
            document.body.classList.remove('dropdown-active')
            document.querySelector('.overlay').classList.remove('active')
        }
    });
})

// on click outside of dropdown, close dropdown menu
window.addEventListener('click', (e) => {
    setTimeout(() => {
        if (document.body.classList.contains('dropdown-active')) {
            if (!e.target.matches('.dropdown-click, .dropdown-click img,.dropdown-click span,.menu__bottom__dropdown__title ,.menu__bottom__dropdown__title h4,.open-dropdown,.open-dropdown ul')) {
                document.querySelector('.open-dropdown')?.classList?.remove('open-dropdown')
                document.querySelector('.dropdown-click')?.classList?.remove('dropdown-click')
                document.body.classList.remove('dropdown-active')
                document.querySelector('.overlay').classList.remove('active')
            }
        }
    }, 100)
})